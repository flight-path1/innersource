# Welcome to InnerSource @ The Smithy
+ **Does your organisation struggle with collaboration across globally federated teams?**
+ **Have you ever attempted to create a document with five people? ten? a hundred?**
+ **Are you an "intrapreneur", disruptor, or tired of the status quo and dreaming of a better way?**

*Look no further, you've arrived at your first waypoint on the journey to better collaboration!*

**Below, Dean Clark, our resident catalyst, welcomes you to this InnerSource experience and pattern!!**

![type:video](./assets/videos/InnerSource at the Smithy.mp4)

*Every aspect of this experience is customisable and open!  Make a recommendation to improve the welcome video [here](http://www.typeform.com).*

##Contribution Creates a Community of Catalysts
You heard Dean introduce the Four C's, but they are worth repeating:

+ Catalyst:
+ Community:
+ Collaboration:
+ Contribution:

Additionally, we believe in Open by Default and embrace the characteristics of Transparency, Inclusivity, and Adaptability made popular by [Red Hat](https://www.youtube.com/watch?v=1aAzwzqqjkI) in open source software development.

##Join Our Adventure
Every community develops it's own traditions, and we are no different.  We want you meaningfully contributing within five minutes of joining.  Take a minute to click through the history of our community.

*insert timeline here*
*Add an event to our timeline through this [form](http://www.typeform.com)*

Head on over to the Getting Started section to learn how to make your first contribution!!

##Maintainers
Dean Clark

Will Watkins

###Contributors
Robert Erenberg-Andersen

Siri Narasimham

Adrian Grenacher

Antony Hyde

##Maintainers
Dean Clark
Will Watkins
##Contributors
Robert Erenberg-Andersen
Siri Narasimham

For full documentation visit [mkdocs.org](https://www.mkdocs.org).
