#Fundamentals of Contribution
##Introduction to Trello
+ **Website**: [Trello](https://trello.com/b/CtmieIfe)
+ **Join the Smithy Workspace**:  [Invite to Board](https://trello.com/invite/b/w6QP0VGM/ATTIb959c586a3156724755b429502f3e4505E5CACF7/innersource)
+ **Start Learning**: [Getting started with Trello](https://trello.com/guide)
+ **Login Information**:
    + **Username**:  Register with your email
    + **Password**:   you'll create it when you sign in
+ **Description**: We use this as a Kanban board to allow novice contributors to collaborate on content and to track issues.

##Introduction to Slack
+ **Website**: [Slack](https://thesmithyworkspace.slack.com)
+ **Join the Smithy Workspace**:  [Invite to Slack](https://join.slack.com/t/thesmithyworkspace/shared_invite/zt-23nwnpaio-eSQ~mn~Yl~xqsKv1rEnNZw)
+ **Start Learning**: [Getting Started](https://slack.com/help/articles/360059928654-How-to-use-Slack--your-quick-start-guide)
+ **Login Information**:
    + **Username**:  Register with your email
    + **Password**:   you'll create it when you sign in
+ **Description**: Slack is our primary communication tool for collaboration.  Once you've joined, please subscribe to <code>**#innersource_collab**</code>.

##Introduction to Markdown
+ **Website**: [Dillinger](https://dillinger.io/)
+ **Start Learning**:  [The Markdown Guide](https://www.markdownguide.org/)
+ **Login Information**:
    + **Username**:  none required
    + **Password**:   none required
+ **Description**: Markdown is a simple and easy-to-use markup language you can use to format virtually any document.  

##Introduction to Material for MKDocs
+ **Website**: [Material for MKDocs](https://squidfunk.github.io/mkdocs-material/)
+ **Start Learning**: [Reference Material for MKDoc](https://squidfunk.github.io/mkdocs-material/reference/)
+ **Login Information**:
    + **Username**:  none required
    + **Password**:   none required
+ **Description**:  Write your documentation in Markdown and create a professional static site in minutes – searchable, customizable, in 60+ languages, for all devices.

##Introduction to Git
###What is Git?

Git is a distributed version control system (DVCS) that tracks changes to files and coordinates work on those files among multiple people. It's widely used in software development and is essential for collaborating on projects with others.

###Key Git Terms and Definitions:

1. **Repository (Repo):** A collection of files and folders that you want to track changes to. It also includes the entire history of changes.
2. **Commit:** A set of changes to files. It's like a snapshot of the files at a certain point in time.
3. **Branch:** A separate line of development. By default, the main branch is called "master" or "main".
4. **Clone:** Creating a copy of a repository.
5. **Fork:** A personal copy of someone else's repository. Useful for proposing changes to someone else's project.
6. **Pull:** Fetching changes from a remote repository.
7. **Push:** Sending your commits to a remote repository.
8. **Pull Request (PR):** Proposing your changes to the owner of the repository. The owner can then "merge" your changes.
9. **Merge:** Applying changes from one branch to another.
10. **Remote:** A version of your repository hosted on the internet (e.g., on platforms like GitHub or Bitbucket).

###Basic Git Commands:

+ **Initializing a repository:**
  <code>git init</code>
+ **Cloning a repository:**
  <code>git clone [URL]</code>
+ **Checking the status of your changes:**
  <code>git status</code>
+ **Adding changes to be committed:**
    + Add all changes:
    <code>git add .</code>
    + Add specific file:
    <code>git add [filename]</code>
+ **Committing your changes:**
    <code>git commit -m "Your commit message here"</code>
+ **Pushing your changes to a remote repository:**
  <code>git push [remote-name] [branch-name]</code>
+ **Pulling changes from a remote repository:**
  <code>git pull [remote-name] [branch-name]</code>
+ **Creating a new branch:**
  <code>git branch [branch-name]</code>
+ **Switching to a different branch:**
  <code>git checkout [branch-name]</code>
+ **Merging one branch into another:**
  <code>git merge [source-branch] [target-branch]</code>

###Example Workflow:
1. **Initialize a new repository:**  
  <code>git init</code>
2. **Make changes to your files. Let's assume you edited example.txt.**
3. **Check your changes:**
<code>git status</code>

    This will show you the files that have been modified and are ready to be committed.

4.  **Add the changes to be committed:**
<code>git add example.txt</code>
5. **Commit the changes:**
<code>git commit -m "Edited example.txt for clarity"</code>
6. If you're working with a remote repository (like on GitHub), you'd then **push your changes** to share them with others:
<code>git push origin master</code>
7. If someone else made changes and you want to get those, you'd **pull those changes**:
<code>git pull origin master</code>

Remember, Git can be complex, but with practice, you'll get the hang of it. Platforms like GitHub also offer graphical interfaces and guides that can simplify many of the processes. Whenever in doubt, always check the Git documentation or seek help from the community.
